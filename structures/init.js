require('dotenv').config()
const Client = require('./HKClient');
const DBCore = require('./HKCore').db;
const fs = Promise.promisifyAll(require('fs'));
const path = require('path');
process.on('unhandledRejection', err => {
	if(err.message && err.message.startsWith('Request timed out')) return;
	try {
		let resp = JSON.parse(err.response);
		if(~[0, 10003, 10008, 40005, 50001, 50013].indexOf(resp.code)) return;
		else throw err;
	} catch(err2) {
		console.error(err.stack);
	}
});
process.on('error', err => console.error(err));
process.on('uncaughtException', err => console.error(err));

(async function init() {
    global.frame = new Client(process.env.TOKEN, {
        disableEvents: { TYPING_START: true },
		messageLimit: 0,
		defaultImageFormat: 'png',
        defaultImageSize: 512,
        prefix: config.get('prefix')
	});
	global.prefix = config.get('prefix');
	global.db = DBCore;
	frame.on('messageCreate', (message) => {
		(async function status() {
			if(message.author.bot || !message.channel.guild) return undefined;
			if(message.content.startsWith(prefix + 'eris-status')) {
				message.channel.createMessage('This bot is working! Using Eris JS Library..');
			}
		})();
	});
	
	frame.connect()
	.then(console.startup('Constructing additional pylons...'))
	.then(await require('./libs/versionVerifier').run).then(console.info('Version verified!'))
	.then(console.startup('Loading core system..'))
	.then(console.startup('This might take a minutes, please wait...'));
})();