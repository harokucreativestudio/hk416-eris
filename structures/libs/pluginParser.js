require('dotenv').config()
global.inject = require('app-root-path').require;
const { readdirSync } = require('fs');
const { Collection } = require('eris');
const { join } = require('path');
const curl = require('node-fetch');
const PluginsMeta = new Collection();

exports.parse = async (frame) => {
	(async function parseMeta(){
		const pluginFolder = readdirSync('./plugins');
		console.startup(`${pluginFolder.length} plugins detected, starting module circular startup...`);
		console.startup(`This may take a minutes, please wait...`);
		for(const meta of pluginFolder) {
			const metadata = readdirSync(`./plugins/${meta}`).filter(x => x.endsWith('.json'));
			for(const file of metadata) {
				let plugin = inject(`./plugins/${file}`);
				console.info(`Loading ${plugin.meta.name} with version ${plugin.meta.version}...`);
				PluginsMeta.set(plugin.meta.name.toLowerCase(), plugin);
		
				(async function parsePlugin(plugin) {
					if(plugin.meta.versionFetch !== disable) {
						versionCheck(plugin)
						.then((body, err) => {
							if(err) return console.error(`There's an error while loading your plugin! : ${err.stack}`);
							if(body === plugin.meta.version) {
								console.info(`You're running latest version of the plugin!`);
								return next;
							} else {
								console.warn(`You're running an outdated version of the plugin!`);
								console.warn(`Consider updating with the latest version of the plugin.`);
								console.warn(`Latest Version : ${body}`);
								return next;
							}
						});
					} else {
						return console.warn(`The developer of current plugin is deactivating version watcher...`);
					}
				})(plugin);

				if(plugin.meta.interpreter === 'command') {
					let internalmeta = inject(join(`./plugins/${file}`, plugin.meta.main)).info;
					frame.on('messageCreate', (message) => {
						
					});
				}
				if(plugin.meta.interpreter === 'event') {

				}
			}
		}
	})();	
}

async function commandParse(frame) {	
		if(message.author.bot || !message.channel.guild) return undefined;
		if(!message.content.startsWith(prefix)) return undefined;
		const args = message.content.slice(frame.config.prefix.length).trim().split(/ +/g);
		const command = args.shift().toLowerCase();
		
		const cmd = frame.commands.get(command) || frame.commands.get(frame.aliases.get(command));
		if(!cmd) return undefined;
		if(!parseCmd(client, msg, message.info)) return undefined;
		return cmd.run(frame, message, args);
}

async function eventParse(frame) {
	for(const event of readdirSync('./events')) {
		frame.on(event.split('.')[0], (...args) => require(`./events/${event}`)(client, ...args));
	}
}

async function versionCheck(plugin) {
	let url = plugin.meta.versionFetch;
	return new Promise((resolve, reject) => {
		curl(url, { 
			method: 'GET'
		})
		.then(res => res.text())
		.then(body => resolve(body))
		.catch(err => {
			console.error(err);
			reject(err);
		});
	})
}

exports.meta = PluginsMeta;

function parseCmd(latina, message, plugin){
	if(plugin.ownerOnly && !latina.config.owners.includes(message.author.id)){
		message.channel.send(` ❌ **| ${message.author.toString()}, ${latina.variables.nickname} can't obey your command, Shikikan-san!**`);
		return false;
	}
	if(plugin.authorPerm.length){
		let perms = [];
		for(const perm of plugin.authorPerm){
			if(!message.member.hasPermission(perm.toUpperCase())) perms.push(perm);
		}
		if(perms.length){
			message.channel.send(`❌ **| ${message.author.toString()}, You lack ${perms.map(x => `▫ | ${x}`).join('\n')} permission nodes to do this, Shikikan-san! \n**`);
			return false;
		}
	}
	if(plugin.latinaPerm){
		let perms = [];
		for(const perm of plugin.latinaPerm){
			if(!message.guild.me.hasPermission(perm.toUpperCase())) perms.push(perm);
		}
		if(perms.length){
			message.channel.send(`❌ **| ${message.author.toString()}, ${latina.variables.nickname} don't have enough permissions to do this, Shikikan-san! \n** ${perms.map(x => `▫ | ${x}`).join('\n')}`);
			return false;
		}
	}
	if(plugin.cooldown){
		const now = Date.now();
		if(!latina.cooldowns.has(plugin.name)) latina.cooldowns.set(plugin.name, new Collection());

		const commandCooldown = latina.cooldowns.get(plugin.name);
		const userCooldown = commandCooldown.get(message.author.id) || 0;
		const estimatedTime = (userCooldown+(plugin.cooldown*1000)) - now;

		if(userCooldown && !latina.config.owners.includes(message.author.id) && estimatedTime > 0){
			message.channel.send(`⏱ **| ${message.author.toString()}, You can use this command again in \`${estimatedTime/1000}s\`**, Shikikan-san!`).catch(e => message.channel.send(e.stack, { code: 'ini' }));
			return false;
		}
		commandCooldown.set(message.author.id, now);
		latina.cooldowns.set(plugin.name, commandCooldown);
	}
	return true;
}