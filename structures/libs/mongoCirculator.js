module.exports = (mongoose, config) => {
	let url = config.get('mongodb');
	mongoose.connect(url, {
		useNewUrlParser: true
	}).then(() => {
		console.info('MongoDB Open Source Driver connected, succesfully synchronized with database...');   
	}).catch(err => {
		console.error('Could not connect to the database. Exiting now...', err);
		process.exit(1);
	});
}