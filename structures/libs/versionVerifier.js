exports.run = async () => {
    const package = inject('./package.json');
    let { softwareversionFetch } = require('./versionUpdater');
    softwareversionFetch()
    .then((latest, err) => {
        if(err) {
            console.warn('Failed to fetch Official Version, continue process using SAFEMODE');
            console.info('https://gate.lolization.space');
            console.info('Copyright 2017 - 2018 Riichi Rusdiana');
            globalSafemode();
        }
        if(package.version !== latest) {
            console.warn('It looks like you are using an Outdated Client or a Patched Version of HK416 Client.');
            console.warn('Remember that it might won\'t work as expected ');
            console.info('https://gate.lolization.space');
            console.info('Copyright 2017 - 2018 Riichi Rusdiana');
            globalSafemode();
        }
        if(package.version == latest) {
            console.startup(`You're using maintained ${package.version} of HKClient`);
            console.info('https://gate.lolization.space');
            console.info('Copyright 2017 - 2018 Riichi Rusdiana');
        }
        console.info(`The latest version of HKClient is ${latest}`);
    });
    function globalSafemode() {
        global.safemode = true;
    }
}
