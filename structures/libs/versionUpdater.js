async function softwareversionFetch() {
    let url = 'https://gitlab.com/harokucreativestudio/hk416-eris/raw/master/package.json';
    return new Promise((resolve, reject) => {
        try {
            curlVersion(resolve);
        } catch(err) {
            reject(err);
        }        
    });

    async function curlVersion(resolve) {
        await require('node-superfetch').get(url).then(response => {
            let body = JSON.parse(response.text);
            let latest = body.version;
            resolve(latest);
        });
    }
}

module.exports = { softwareversionFetch };