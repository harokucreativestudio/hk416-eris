require('dotenv').config()
const Eris = require("eris-additions")(require("eris"),
	{ enabled: ["Channel.awaitMessages", "Member.bannable", "Member.kickable", "Member.punishable", "Role.addable"] }
);
const logger = require('./libs/logEngine').logger;
const mongoose = require('mongoose');
const configConstructor = require('./libs/configConstructor.js');
const Config = new configConstructor();
const embedConstructor = require('./libs/embedConstructor');

(function globalScope() {
	global.Promise = require('bluebird');
    mongoose.Promise = global.Promise;
    global.inject = require('app-root-path').require;
    global.config = Config;
    global.embedConstructor = embedConstructor;
    if(config.get('database') === 'mongodb') {
        return require('./libs/mongoCirculator')(mongoose, global.config);
    } else if(config.get('database') === 'mysql') {
        return console.info('Interpreter Mysql DB is not properly set up yet..');
    } else if(config.get('database') === 'nodb') {
        return console.info('You\'re using nodb mode, some plugin might not working properly...');
    }
    
})();

exports.HKCore = Eris;
exports.logger = logger;
exports.db = mongoose;
exports.configConstructor = configConstructor;
