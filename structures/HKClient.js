require('dotenv').config()
const HKCore = require('./HKCore').HKCore;

class HKClient extends HKCore {
    constructor(...args) {
        super(...args);

        this.config = global.config;
    }
}

module.exports = HKClient;